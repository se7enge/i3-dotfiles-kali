#!/bin/bash

# Terminate already running bar instances:
/usr/bin/polybar-msg cmd quit

for monitor in $(/usr/bin/polybar -m | /usr/bin/cut -d ":" -f1); do
    MONITOR=$monitor /usr/bin/polybar -r -q main & disown

done
