#!/bin/sh

nm-applet &                                                                      # start network manager applet
pulseaudio &                                                                     # start sound server
xfce4-power-manager &                                                            # start power manager
dunst &                                                                          # start notification daemon
nitrogen --set-zoom-fill $HOME/.local/share/backgrounds/kali-2.0-2560x1440.png & # set the wallpaper

# NOTE: The following command is a workaround for using betterlockscreen in a live build
# copied from 'https://gitlab.com/Arszilla/i3-dotfiles'. Released under an MIT License.
xrandr | grep -w connected | awk '{print "-u '$HOME/.local/share/backgrounds/kali-1.0-1920x1080.png' --display " $1}' | xargs betterlockscreen &

# NOTE: This may potentially weaken your security - do your own research.
# Uncomment the following line if using vmware and functionality like copy/paste between host/guest is desired:
#killall -q vmware-user-suid-wrapper
#while pgrep -u $UID -x vmware-user-suid-wrapper >/dev/null; do sleep 1; done
#vmware-user-suid-wrapper &

# NOTE: This may potentially weaken your security - do your own research.
# Uncomment the following lines if using KVM/QEMU and functionality like copy/paste between host/guest is desired:
#killall -q spice-vdagent
#while pgrep -u $UID -x spice-vdagent >/dev/null; do sleep 1; done
#spice-vdagent -d &

#autotiling &                                                                     # start autotiling
$HOME/.config/i3/scripts.d/autotiling.py &                                       # start autotiling from $HOME
$HOME/.config/polybar/launch.sh &                                                # start polybar

# Uncomment the following lines if you want compositing:
#killall -q picom                                                                 # kill picom if already running
#while pgrep -u $UID -x picom >/dev/null; do sleep 1; done                        # wait for picom to stop
#picom  --config $HOME/.config/picom/picom.conf &                                 # start picom

/usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1 &                 # start polkit
