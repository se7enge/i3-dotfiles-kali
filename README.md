# i3 Dotfiles for Kali Linux

## Description
These are my custom dotfiles for Offensive Security's Kali Linux, utilising the i3 window manager as a more lightweight, efficient, and keyboard driven alternative to the otherwise officially supported default graphical environments (XFCE, Gnome, and KDE).

## Philosophy / Motivation
**Why use this when Offensive Security now has an official i3 package in their repo?**

+ The official i3 package for Kali uses a pre-configuration with heavy compositing that makes somewhat redundant the speed and efficiency of a lightweight window manager, such as i3, and does not function as well as it otherwise should on resource conservative machines. This build, however, works out the box with _zero_ compositing and is therefore faster and more responsive and has a greater degree of compatibility with older and/or more resource conservative machines, virtual or otherwise.

+ The official package utilises some of i3's default keybindings which many users find frustrating and end up changing anyway, often to some 'vim-like' bindings which many users are already familiar with and which many other window managers already support out the box. This configuration supports these 'saner defaults' from the get go, taking advantage of 'vim-like' keybindings, including the use of custom 'vim-like' binds for (text input) inside Gtk apps.

+ The official package attempts to provide a more 'glossy' modern look and feel, and while there is value in that - particularly for general use machines - the primary purpose of Kali Linux is, generally speaking, considered to be that of a professional toolset for security/penetration testing and _not_ as a daily driver for general use - although it can of course be used as such if desired. This configuration, therefore, is designed to be more 'classic' and minimal in look and feel, in such as way that prioritises function over form, substance over style, and efficient usability, whilst not completely sacrificing aesthetics.

## Dependencies
`kali-desktop-core`, `kali-system-cli`, `i3-wm`, `i3lock-color`, `i3status`, `python3-i3ipc`, `xorg`, `pulseaudio`, `alsa-utils`, `brightnessctl`, `nitrogen`, `betterlockscreen`, `dunst`, `polybar`, `rofi`, `sxiv`, `maim`, `xclip`, `xdotool`, `rxvt-unicode`, `zathura`, `pcmanfm`, `htop`, `neofetch`, `xfce4-power-manager`, `network-manager`, `network-manager-gnome`, `webext-ublock-origin-firefox`

## Installation
For a pre-existing installation of Kali, (having satisfied dependencies) simply clone the repository and copy the files into your `$HOME` directory:

```
$ git clone https://gitlab.com/se7enge/i3-dotfiles-kali.git
$ cd i3-dotfiles-kali/
$ cp -r . ~/
```

_A live build configuration is in the works and should hopefully be available in the near future._

## Notes
This configuration assumes the use of the `startx` command for initiating the graphical environment (after successful login) without the need for additional software (such as a display manager) in order to keep things lightweight and minimal. This can be useful if the task you need to perform is simple and doesn't require any graphical software as you will be dropped straight into a terminal (in a tty) and can begin getting to work immediately without needing to enter a graphical environment (if you wish).

If you prefer a display/login manager, then you will have to configure this yourself. You may already have one, such as `lightdm`, if you installed Kali with one of the default graphical environments selected during the installation process.
