"                    .                    
"    ##############..... ##############   
"    ##############......##############   
"      ##########..........##########     
"      ##########........##########       
"      ##########.......##########        
"      ##########.....##########..        
"      ##########....##########.....      
"    ..##########..##########.........    
"  ....##########.#########.............  
"    ..################JJJ............    
"      ################.............      
"      ##############.JJJ.JJJJJJJJJJ      
"      ############...JJ...JJ..JJ  JJ     
"      ##########....JJ...JJ..JJ  JJ      
"      ########......JJJ..JJJ JJJ JJJ     
"      ######    .........                
"                  .....                  
"                    .           

" Get the defaults that most users want
source $VIMRUNTIME/defaults.vim

" Enable line numbering
set number

" Enable cursorline (w/o full line highlight, i.e. for number only)
set cursorline
set cursorlineopt=number

" Set tab size explicitly
set tabstop=4
set shiftwidth=4

" Make searching case insensitive unless uppercase is specified
set ignorecase
set smartcase

" Enable copy/paste between vim and other programs
set clipboard=unamedplus

" Map 'Ctrl+Backspace' to word rubout
"inoremap <C-H> <C-W>

" Set term colors
set t_Co=256
set bg=dark

" Set line number colors
highlight LineNr           ctermfg=8    ctermbg=none    cterm=none
highlight CursorLineNr     ctermfg=7    ctermbg=none       cterm=none

" Enable spell checking in markdown
autocmd FileType markdown setlocal spell spelllang=en_us
